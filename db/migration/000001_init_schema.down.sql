/*
  We drop entries and tranfers before accounts since there are foreign
  key constraints from accounts in both tables.
*/
DROP TABLE IF EXISTS entries;
DROP TABLE IF EXISTS transfers;
DROP TABLE IF EXISTS accounts;